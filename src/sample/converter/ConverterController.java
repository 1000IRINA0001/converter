package sample.converter;

import sample.converter.assets.Shake;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * класс, демонстрирующий работу конвертера
 *
 * @author  Прокофьева И.А.
 */
@SuppressWarnings("unused")
public class ConverterController {
    private static final String INPUT_ERROR_MESSAGE = "Введите число";//сообщение об ошибке
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField inputLengthTextField;

    @FXML
    private TextField resultLengthTextField;

    @FXML
    private ComboBox<?> firstLengthComboBox;

    @FXML
    private ComboBox<?> secondLengthComboBox;

    @FXML
    private Label lengthErrorLabel;

    @FXML
    private Button resultLengthButton;

    @FXML
    private Button clearLengthButton;

    @FXML
    private TextField inputWeightTextField;

    @FXML
    private TextField resultWeightTextField;

    @FXML
    private ComboBox<?> firstWeightComboBox;

    @FXML
    private ComboBox<?> secondWeightComboBox;

    @FXML
    private Label weightErrorLabel;

    @FXML
    private Button resultWeightButton;

    @FXML
    private Button clearWeightButton;

    @FXML
    private TextField inputTemperatureTextField;

    @FXML
    private TextField resultTemperatureTextField;

    @FXML
    private ComboBox<?> secondTemperatureComboBox;

    @FXML
    private ComboBox<?> firstTemperatureComboBox;

    @FXML
    private Label currencyErrorLabel;

    @FXML
    private Button resultTemperatureButton;

    @FXML
    private Button clearTemperatureButton;

    /**
     * метод очистки comboBox и текстовых полей в конвертере длины
     * @param event нажатие на кнопку "очистить"
     */
    @FXML
    void clearLength(ActionEvent event) {
        inputLengthTextField.clear();
        resultLengthTextField.clear();
        firstLengthComboBox.setValue(null);
        secondLengthComboBox.setValue(null);
    }

    /**
     *метод очистки comboBox и текстовых полей в конвертере температуры
     * @param event нажатие на кнопку "очистить"
     */
    @FXML
    void clearTemperature(ActionEvent event) {
        inputTemperatureTextField.clear();
        resultTemperatureTextField.clear();
        firstTemperatureComboBox.setValue(null);
        secondTemperatureComboBox.setValue(null);
    }

    /**
     *метод очистки comboBox и текстовых полей в конвертере массы
     * @param event нажатие на кнопку "очистить"
     */
    @FXML
    void clearWeight(ActionEvent event) {
        inputWeightTextField.clear();
        resultWeightTextField.clear();
        firstWeightComboBox.setValue(null);
        secondWeightComboBox.setValue(null);
    }

    /**
     * метод конвертирует единицы длины(определяет введенный в comboBox элеметы и выполняет условие перевода)
     * @param event нажатие на кнопку "Результат"
     */
    @FXML
    void convertsLength(ActionEvent event) {
        try {
            //  errors();
            double result = Integer.valueOf(inputLengthTextField.getText());
            if ("мм".equals(secondLengthComboBox.getValue())) {
                if ("см".equals(firstLengthComboBox.getValue())) {
                    result /= 10;
                }
                if ("дм".equals(firstLengthComboBox.getValue())) {
                    result /= 100;
                }
                if ("м".equals(firstLengthComboBox.getValue())) {
                    result /= 1000;
                }
                if ("км".equals(firstLengthComboBox.getValue())) {
                    result /= 1000000;
                }
            }
            if ("см".equals(secondLengthComboBox.getValue())) {
                if ("мм".equals(firstLengthComboBox.getValue())) {
                    result *= 10;
                }
                if ("дм".equals(firstLengthComboBox.getValue())) {
                    result /= 100;
                }
                if ("м".equals(firstLengthComboBox.getValue())) {
                    result /= 100;
                }
                if ("км".equals(firstLengthComboBox.getValue())) {
                    result /= 100000;
                }
            }
            if ("дм".equals(secondLengthComboBox.getValue())) {
                if ("мм".equals(firstLengthComboBox.getValue())) {
                    result *= 100;
                }
                if ("см".equals(firstLengthComboBox.getValue())) {
                    result *= 10;
                }
                if ("м".equals(firstLengthComboBox.getValue())) {
                    result /= 10;
                }
                if ("км".equals(firstLengthComboBox.getValue())) {
                    result /= 10000;
                }
            }
            if ("м".equals(secondLengthComboBox.getValue())) {
                if ("см".equals(firstLengthComboBox.getValue())) {
                    result *= 100;
                }
                if ("мм".equals(firstLengthComboBox.getValue())) {
                    result *= 1000;
                }
                if ("дм".equals(firstLengthComboBox.getValue())) {
                    result *= 10;
                }
                if ("км".equals(firstLengthComboBox.getValue())) {
                    result /= 1000;
                }
            }
            if (" км".equals(secondLengthComboBox.getValue())) {
                if ("мм".equals(firstLengthComboBox.getValue())) {
                    result *= 1000000;
                }
                if ("см".equals(firstLengthComboBox.getValue())) {
                    result *= 100000;
                }
                if ("дм".equals(firstLengthComboBox.getValue())) {
                    result *= 10000;
                }
                if ("м".equals(firstLengthComboBox.getValue())) {
                    result *= 1000;
                }
            }
            resultLengthTextField.setText(String.valueOf(result));
        } catch (Exception e) {
            inputLengthTextField.setText(INPUT_ERROR_MESSAGE);
        }

    }

    /**
     *метод конвертирует температуру(определяет введенный в comboBox элеметы и выполняет условие перевода)
     * @param event нажатие на кнопку "Результат"
     */
    @FXML
    void convertsTemperature(ActionEvent event) {
        try {
            errors();
            double result = Integer.valueOf(inputTemperatureTextField.getText());
            if ("Цельсий".equals(secondTemperatureComboBox.getValue())) {
                if ("Фаренгейт".equals(firstTemperatureComboBox.getValue())) {
                    result = (result - 32) / 1.8;
                }

                if ("Кельвин".equals(firstTemperatureComboBox.getValue())) {
                    result -= 274.15;
                }
            }
            if ("Фаренгейт".equals(secondTemperatureComboBox.getValue())) {
                if ("Кельвин".equals(firstTemperatureComboBox.getValue())) {
                    result = result * 1 / 8 - 459.67;
                }
                if ("Цельсий".equals(firstTemperatureComboBox.getValue())) {
                    result = result * 1.8 + 32;
                }
            }
            if ("Кельвин".equals(secondTemperatureComboBox.getValue())) {
                if ("Фаренгейт".equals(firstTemperatureComboBox.getValue())) {
                    result = (result + 459.67) / 1.8;
                }
                if ("Цельсий".equals(firstTemperatureComboBox.getValue())) {
                    result += 273;
                }
            }
            resultTemperatureTextField.setText(String.valueOf(result));
        } catch (Exception e) {
            inputTemperatureTextField.setText(INPUT_ERROR_MESSAGE);
        }

    }

    /**
     * метод конвертирует единицы массы(определяет введенный в comboBox элеметы и выполняет условие перевода)
     * @param event нажатие на кнопку "Результат"
     */
    @FXML
    void convertsWeight(ActionEvent event) {
        try {
            errors();
            double result = Integer.valueOf(inputWeightTextField.getText());
            if ("мг".equals(secondWeightComboBox.getValue())) {
                if ("г".equals(firstWeightComboBox.getValue())) {
                    result *= 1000;
                }

                if ("кг".equals(firstWeightComboBox.getValue())) {
                    result *= 1000000;
                }
                if ("т".equals(firstWeightComboBox.getValue())) {
                    result *= 1000000000;
                }

            }
            if ("г".equals(secondWeightComboBox.getValue())) {
                if ("мг".equals(firstWeightComboBox.getValue())) {
                    result /= 0.001;
                }
                if ("кг".equals(firstWeightComboBox.getValue())) {
                    result *= 1000;
                }
                if ("т".equals(firstWeightComboBox.getValue())) {
                    result *= 1000000;
                }

            }
            if ("кг".equals(secondWeightComboBox.getValue())) {
                if ("мг".equals(firstWeightComboBox.getValue())) {
                    result *= 0.000001;
                }
                if ("г".equals(firstWeightComboBox.getValue())) {
                    result *= 0.001;
                }
                if ("т".equals(firstWeightComboBox.getValue())) {
                    result *= 1000;
                }
            }
            if (" т".equals(secondWeightComboBox.getValue())) {
                if ("мг".equals(firstWeightComboBox.getValue())) {
                    result *= 0.000000001;
                }
                if ("г".equals(firstWeightComboBox.getValue())) {
                    result *= 0.000001;
                }
                if ("кг".equals(firstWeightComboBox.getValue())) {
                    result *= 0.001;
                }
            }
            resultWeightTextField.setText(String.valueOf(result));
        } catch (Exception e) {
            inputWeightTextField.setText(INPUT_ERROR_MESSAGE);
        }

    }


    /**
     * метод срабатывает,если не введено число( выводит текст об ошибке; срабатывает анимация)
     */
    private void errors() {
        if (inputTemperatureTextField.getText().isEmpty() ||
                inputWeightTextField.getText().isEmpty() ||
                inputLengthTextField.getText().isEmpty()) {
            Shake firstLengthAnim = new Shake(inputLengthTextField);
            Shake firstTemperatureAnim = new Shake(inputTemperatureTextField);
            Shake firstWeigthAnim = new Shake(inputWeightTextField);

            firstLengthAnim.playAnimation();
            firstTemperatureAnim.playAnimation();
            firstWeigthAnim.playAnimation();

            return;
        }
    }

    /**
     * метод анимации
     * @param inputTextFild текстовое поле
     */
    private void animation(TextField inputTextFild) {
        Shake operandAnimation = new Shake(inputTextFild);
        operandAnimation.playAnimation();
    }

}

