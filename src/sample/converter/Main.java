package sample.converter;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("converter.fxml"));
        primaryStage.getIcons().add(new Image("sample\\converter\\assets\\icon2.png"));
        primaryStage.setTitle("Конвертер");
        primaryStage.setScene(new Scene(root, 385, 339));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}

